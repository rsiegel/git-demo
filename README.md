# Welcome to ARC Software Engineering

This repository contains code for a fictional ARC Software Engineering class,
which happens to be much easier than the Computer Science department's offering.

In this class, we will develop a bash script, incrementally adding requirements
that increase the complexity of the script.


## Requirements
- Sprint 1: The script must print the string "Hello, World!" to the shell.
- Sprint 2: The script must accept an argument of who to greet. If no argument
  is provided, World should be greeted.
- Sprint 3: Refactor your code. You should only use the `echo` command once.